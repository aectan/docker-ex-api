# Старт проекта

## Клонировать репозитории

```
git clone https://gitlab.com/aectan/api-server.git
git clone https://gitlab.com/aectan/auto-api-client.git
```

## Создать .env

```
POSTGRES_HOST=db
POSTGRES_USER=user
POSTGRES_PASSWORD=password
RAILS_ENV=development
```

## Запустить сборку
`docker-compose up`

## Создание базы данных
`docker-compose exec api rake db:create db:migrate db:seed`
